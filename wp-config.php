<?php
/** Enable W3 Total Cache */
define('WP_CACHE', false); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stamponegroup');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/w?u]a]`PMm<sm]rwe^stl1MqLu+9W2g<#!#L:WLR0mV)F#pmpU,1~g:[nKg@,b[');
define('SECURE_AUTH_KEY',  '`K1J9%<PJYO* 5:iJl-VA{(!]:Tj!oZ.)|a}v*78$?5G dO)[!CQWjQ=3_O1vRc-');
define('LOGGED_IN_KEY',    'ka%_+Ghe*W=$ZNC|-c_m3|z{|PFR+KiWf/VE@>:`NkH+hEC 2_?]$uxS@O^!=x{5');
define('NONCE_KEY',        ')f,M)&UnNJteGEL=3.<Rss`MD/Kf|gWfhDES#[1U2Y#]V.sw+~@2AFdb%Xx_sK|n');
define('AUTH_SALT',        'nM zMJE3jm-^gdtUkNSc|PS(0/5w;|a8@s@Q5A/3D$8-4b:sV&M5.V%o84*C&iXl');
define('SECURE_AUTH_SALT', '94> ,kzivy@+Y1=~u|_Zm:D3otA/./2u_^o.|h<HYwb6j*^[${W@;,Ex-pQVw!R%');
define('LOGGED_IN_SALT',   'DU+5>Sdbw9xR*K(|&<ga rM-Nx-+Z1ADr_zH?PbL|k(h32LJV.vLjkHZ7~JRf+E`');
define('NONCE_SALT',       '6Vf?v+y-OhIeiSzus3Y-iuhR_.-g$q<87A.f-DIFQnVKb%_()tDsAUl+v9+ATt+T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cju_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
