<?php
/**
 * Index Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
if (!empty($_GET['search-listings'])) {
	require('search-listings.php');
	return;
}

get_header();
            
			
			get_template_part('/includes/advanced-search');
			
			
			echo '<section class="featured-listings">';
				echo '<div class="container">';
					get_template_part('/includes/featured-listings');
				echo'</div>';
			echo'</section>';
			echo '<div class="clear"></div>';
			
	
get_footer(); ?>