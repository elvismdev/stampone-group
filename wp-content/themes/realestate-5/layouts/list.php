<?php 
/**
 * Listings Loop
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */

if ( ! $wp_query->have_posts() ) : ?>

    <p class="nomatches"><strong><?php _e( 'No properties were found which match your search criteria', 'contempo' ); ?></strong>.<br /><?php _e( 'Try broadening your search to find more results.', 'contempo' ); ?></p>

<?php elseif ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

    <article class="listing">
        <figure class="col span_5">
            <?php ct_status(); ?>
            <?php ct_first_image_linked_view(); ?>
        </figure>
        <div class="listing-info col span_7">
            <h2 class="marB0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p class="location marB18"><?php city(); ?>, <?php state(); ?> <?php zipcode(); ?></p>
            <?php if( (get_post_meta($post->ID, "_ct_price", true)) != "" ) { ?><h3 class="price marB18"><strong><?php currency(); ?><?php listing_price(); ?></strong><?php if(has_status('for-rent') || has_status('rental')) { echo " /month"; } ?></h3><?php } ?>
            <p class="propinfo marB10"><?php if(has_type('land') || has_type('lot')) { ?><?php echo get_post_meta($post->ID, "lotsize_value", true); ?> <?php acres(); ?><?php } elseif(has_type('commercial')) { ?><?php // Display Nothing ?><?php } else { ?><?php beds(); ?> <?php baths(); ?> <?php if((get_post_meta($post->ID, "_ct_sqft", true))) { ?> | <?php echo get_post_meta($post->ID, "_ct_sqft", true); ?> <?php sqftsqm(); ?><?php } ?> <?php  if((get_post_meta($post->ID, "_ct_lotsize", true)) != "") { ?><?php _e('on', 'contempo'); ?> <?php echo get_post_meta($post->ID, "_ct_lotsize", true); ?> <?php acres(); ?><?php } ?><?php } ?></p>
            <p class="agent marB0"><?php propertytype(); ?> | <?php _e('Listing Agent:', 'contempo'); ?> <a href="<?php echo site_url(); ?>/?author=<?php the_author_meta('ID'); ?>"><?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?></a></p>
        </div>
            <div class="clear"></div>
    </article>
    
<?php endwhile; ?>

	<?php ct_listings_nav(); ?>

<?php endif; ?>