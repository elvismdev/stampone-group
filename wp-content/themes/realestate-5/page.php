<?php
/**
 * Page Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

get_header();

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo the_title();
		echo '</h1>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<div class="container">';

		if($ct_options['ct_layout'] == 'left-sidebar') {
			get_sidebar();
		}

		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article class="col span_9 <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">
            
			<?php the_content(); ?>
            
            <?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'contempo' ) . '</span>', 'after' => '</div>' ) ); ?>
            
            <?php endwhile; endif; wp_reset_query(); ?>
            
                <div class="clear"></div>

        </article>

		<?php if($ct_options['ct_layout'] == 'right-sidebar') {
			get_sidebar();
		}

echo '</div>';

get_footer(); ?>