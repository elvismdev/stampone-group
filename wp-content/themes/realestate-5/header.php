<?php
/**
 * Header Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */

// Load Theme Options
global $ct_options;

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="<?php language_attributes(); ?>"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="<?php language_attributes(); ?>"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="<?php language_attributes(); ?>"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?php language_attributes(); ?>"><!--<![endif]-->
<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php ct_title(); ?></title>
	

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
    <?php wp_enqueue_script("jquery"); ?>

	<?php wp_head(); ?>
    
</head>

<body<?php ct_body_id('top'); ?> <?php body_class(); ?>>

<?php if($ct_options['ct_boxed'] == "Boxed") {
echo '<div class="container main">';
} ?>
    
    <div id="wrapper" <?php if($ct_options['ct_boxed'] == "Boxed") { echo 'class="boxed"'; } ?>>
        
        <header id="masthead" <?php if($ct_options['ct_boxed'] == "Boxed") { echo 'class="boxed"'; } ?>>
        
           <div class="container">

                        <a href="<?php echo home_url(); ?>"><img class="logo left" src="<?php echo get_template_directory_uri(); ?>/images/re5-logo.png" alt="WP Pro Real Estate 5, a WordPress theme by Contempo" /></a>

                <div class="right">
					<?php ct_nav(); ?>
                    
                    <ul id="ct-search" class="right">
                        <li>
                            <a class="search"><i class="icon-search"></i></a>
                            <?php get_template_part('searchform'); ?>
                        </li>
                    </ul>
                </div>
                
                    <div class="clear"></div>   
            </div>
        </header>

<?php if(is_home()) { ?>
<div class="callout">
<div class="container">
<h1>If you’re seeking South Florida’s finest luxury homes with unrelenting personal service—you’ve come to the right place. Welcome to Stampone Group.</h1>

<div class="tube"><iframe width="560" height="315" src="//www.youtube.com/embed/tPbQbs9sHck?rel=0&controls=0&showinfo=0&modestbranding=1&autohide=1" frameborder="0" allowfullscreen></iframe></div>
</div>
</div>

<div class="subscribebar">
<div class="container">
<div class="col1">
<p>Say 'YES' to luxury living. Stay in the know about the latest happenings in the South Florida luxury real estate market.</p>
</div>
<div class="col2">

<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="http://stamponegroup.us7.list-manage2.com/subscribe/post?u=c39f76751528d6a38de9186ee&amp;id=34c395133b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="os">
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="Email Address" onfocus="if (this.value == 'Email Address') {this.value=''}" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
</div>
<div class="ts">
<div class="mc-field-group">
	<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
</label>
	<input type="text" value="First Name" onfocus="if (this.value == 'First Name') {this.value=''}" name="FNAME" class="required" id="mce-FNAME">
</div>
</div>
<div style="clear:both;"></div>
<div class="os">
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="Last Name" onfocus="if (this.value == 'Last Name') {this.value=''}" name="LNAME" class="" id="mce-LNAME">
</div>
</div>
<div class="ts">
<div class="mc-field-group">
	<label for="mce-MMERGE3">Zip Code  <span class="asterisk">*</span>
</label>
	<input type="text" value="Zip Code" onfocus="if (this.value == 'Zip Code') {this.value=''}" name="MMERGE3" class="required" id="mce-MMERGE3">
</div>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c39f76751528d6a38de9186ee_34c395133b" value=""></div>
	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>

<!--End mc_embed_signup-->
</div>
</div>
</div>
<?php } ?>