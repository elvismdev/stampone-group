<?php
/**
 * Template Name: Big Map
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

get_header();

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo the_title();
		echo '</h1>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<div class="container">';

		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
        <article class="col span_12">
            
			<?php the_content(); ?>
            
            <?php multi_marker_map(); ?>
            
            <?php endwhile; endif; ?>
            
                <div class="clear"></div>

        </article>

<?php 

echo '</div>';

get_footer(); ?>