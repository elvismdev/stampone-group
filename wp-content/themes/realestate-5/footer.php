<?php
/**
 * Footer Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options;

?>
            <div class="clear"></div>
       
        <footer>
            <div class="container">   
                    
                    <?php wp_nav_menu(array('menu' => 'footer', 'container' => 'left')); ?>
              
                    <p class="right">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>, <?php _e( 'All Rights Reserved.', 'contempo' ); ?> <a id="back-to-top" href="#top"><?php _e( 'Back to top &uarr;', 'contempo' ); ?></a></p>

            </div>
        </footer>
    
    <?php if($ct_options['ct_boxed'] == "Boxed") {
	echo '</div>';
	} ?>
    
    <?php if($ct_options['ct_tracking_code']) { ?>
        <?php echo stripslashes($ct_options['ct_tracking_code']); ?>
    <?php } ?>

	<?php wp_footer(); ?>
</body>
</html>