<?php
/**
 * Template Name: Testimonials
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

get_header();

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo the_title();
		echo '</h1>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<section class="testimonial-wrap">';
	echo '<div class="container"> '; 
	echo $ct_options['ct_testimonial_lead'];                  
		get_template_part('/includes/testimonials');
	echo '<div class="clear"></div>';
	echo '</div>';                    
echo '</section>';

get_footer(); ?>