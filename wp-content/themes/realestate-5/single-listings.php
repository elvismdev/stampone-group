<?php
/**
 * Single Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options;

get_header();
 
if (!empty($_GET['search-listings'])) {
	require('search-listings.php');
	return;
}

$cat = get_the_category();
$cat = $cat[0];

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<div class="left">';
		echo '<h1 class="marB0">';
		echo the_title();
		echo '</h1>';
		echo '<h3 class="marB0">';
		echo '&nbsp;' . city();
		echo '&nbsp;' . state();
		echo '&nbsp;' . zipcode();
		echo '</h3>';
		echo '</div>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<div class="container">';

		if ( have_posts() ) : while ( have_posts() ) : the_post();

		if($ct_options['ct_layout'] == 'left-sidebar') {
			echo '<div id="sidebar" class="col span_3">';
				if (function_exists('dynamic_sidebar') && dynamic_sidebar('Listing Single Left') ) :else: endif;
			echo '</div>';
		} ?>

        <article class="col span_9 <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">
            
            <?php if( (get_post_meta($post->ID, "_ct_price", true)) != "" ) { ?><h2 class="price marB18"><strong><?php currency(); ?><?php listing_price(); ?></strong><?php if(has_status('for-rent') || has_status('rental')) { echo " /month"; } ?></h2><?php } ?>
            
            <p class="propinfo marB20">
            
				<?php                 
					if(has_type('commercial')) { 
						propertytype(); 
						echo ' | ';
					} else {
						beds(); baths();
					}
					
					if((get_post_meta($post->ID, "_ct_sqft", true))) {
						echo ' | ';
						echo get_post_meta($post->ID, "_ct_sqft", true);
						echo ' ';
						sqftsqm();
					}
					
					if((get_post_meta($post->ID, "_ct_lotsize", true))) {
						if((get_post_meta($post->ID, "_ct_sqft", true))) {
							echo ' | ';
						}
						echo get_post_meta($post->ID, "_ct_lotsize", true);
						echo ' ';
						acres();
					}
				?>
                 
            </p>
            
            <figure>
				<?php
                $attachments = get_children(
                    array(
                        'post_type' => 'attachment',
                        'post_mime_type' => 'image',
                        'post_parent' => $post->ID
                    ));
                if(count($attachments) > 1) { ?>
                    <div id="slider" class="flexslider">
                        <?php ct_status(); ?>
                        <ul class="slides">
                            <?php ct_slider_images(); ?>
                        </ul>
                    </div>
                    <div id="carousel" class="flexslider">
                        <ul class="slides">
                            <?php ct_slider_carousel_images(); ?>
                        </ul>
                    </div>
                <?php } else {
                    ct_first_image_lrg();
                } ?>
            </figure>   
            
            <div class="post-content marT20">
				<?php the_content(); ?>
                
                <?php addfeatlist(); ?>
                
                <?php if(get_post_meta($post->ID, "_ct_video", true)) { ?>
                <div class="videoplayer marB20">
                    <h4 class="border-bottom marB20"><?php _e('Video', 'contempo'); ?></h4>
                    <?php echo stripslashes(get_post_meta($post->ID, "_ct_video", true)); ?>
                </div>       
                <?php } ?>
                
                <?php if($ct_options['ct_disable_google_maps'] == 'No') { ?>
                <div id="location">
                    <h4 class="border-bottom marB18"><?php _e('Location', 'contempo'); ?></h4>
                    <?php listing_map(); ?>
                </div>  
                <?php } ?>
            </div>       
            
            <?php endwhile; endif; ?>
            
                <div class="clear"></div>
            
            <?php ct_post_nav(); ?>
            
            <?php if($ct_options['ct_post_comments'] == "Yes" && comments_open()) { ?>
				<?php comments_template( '', true ); ?>
            <?php } ?>

        </article>
        
        <?php if($ct_options['ct_layout'] == 'right-sidebar') {
			echo '<div id="sidebar" class="col span_3">';
				if (function_exists('dynamic_sidebar') && dynamic_sidebar('Listing Single Right') ) :else: endif;
			echo '</div>';
		}

echo '</div>';

get_footer(); ?>