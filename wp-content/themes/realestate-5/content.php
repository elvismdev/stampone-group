<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

$post_lead = $ct_options['ct_post_thumb'];
$post_social = $ct_options['ct_post_social'];

if(is_single()) { 
        
	if(get_post_meta($post->ID, "_ct_video", true)) {
		echo '<div class="video marB40">';
		echo stripslashes(get_post_meta($post->ID, "_ct_video", true));
		echo '</div>'; 
	}
	
	echo '<div class="entry-header">';
	
		if($post_lead == 'Yes') {
			echo '<div class="post-thumb">';
			$attachments = get_children(
				array(
					'post_type' => 'attachment',
					'post_mime_type' => 'image',
					'post_parent' => $post->ID
				));
			if(count($attachments) > 1) {
				echo '<div class="flexslider">';
				echo	'<ul class="slides">';
							ct_slider_images();
				echo	'</ul>';
				echo '</div>';
			} elseif(has_post_thumbnail()) {
				echo '<figure>';
				the_post_thumbnail(620,200);  
				echo '</figure>';
			}
			echo '</div>';		
		}
		
		if($post_title == 'Yes') {
			echo '<header id="post-title">';
				echo '<small>';
				echo the_time($GLOBALS['ctdate']);
				echo '</small>';
				echo '<h1 class="marB5">';
				echo the_title();
				echo '</h1>';
			echo '</header>';
		}
	
	echo '</div>';

	include(TEMPLATEPATH . '/includes/post-meta.php');
	
	the_content();
	
	if($post_social == 'Yes') {
		include(TEMPLATEPATH . '/includes/post-social.php');
	}
	
	ct_related_posts();

} else { ?>
    
    <?php if(has_post_thumbnail()) { ?>
        <article <?php post_class(); ?>>
    <?php } else { ?>
        <article class="no-thumb">
    <?php } ?>
        
        <?php if(has_post_thumbnail()) { ?>
            <figure col span_12">
                <a class="thumb" href="<?php the_permalink() ?>">
                    <?php the_post_thumbnail(620,200); ?>
                </a>
            </figure>
        <?php } ?>
    
        <div class="content col span_8">
            <small><?php the_time($GLOBALS['ctdate']); ?></small>
            <h2 class="title marT10 marB5"><a href="<?php the_permalink() ?>"><span data-title="<?php the_title(); ?>"><?php the_title(); ?></span></a></h2>
            <div class="excerpt marT20 marB20">
                <?php the_excerpt(); ?>
            </div>
            <a class="more" href="<?php the_permalink(); ?>"><?php _e('More', 'contempo'); ?></a>
        </div>
            <div class="clear"></div>  
    </article>
    <?php include(TEMPLATEPATH . '/includes/post-meta.php'); ?>

<?php } ?>

<?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'contempo' ) . '</span>', 'after' => '</div>' ) ); ?>    