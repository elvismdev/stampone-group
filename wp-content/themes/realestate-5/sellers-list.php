<?php
/**
 * Template Name: Sellers Listing
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

?>

<?php get_header(); ?>

<header id="archive-header" class="marB40">
<div class="container">
<h1 class="marB0 left"><?php the_title(); ?></h1>
<?php ct_breadcrumbs(); ?>
<div class="clear"></div>
</div>
</header>

<div class="container">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article class="col span_9 sellerlist <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">

<?php if(get_field('welcometext')) { ?>
<h2 class="welcometext"><?php the_field('welcometext'); ?></h2>
<?php } ?>

<?php if(get_field('video')) { ?>
<div class="vidspot">
<iframe width="100%" height="240" src="//www.youtube.com/embed/<?php the_field('video'); ?>?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
<?php } ?>

<div style="clear:both;"></div>

<?php the_content(); ?>

<?php endwhile; endif; wp_reset_query(); ?>
<div class="clear"></div>

<?php if(get_field('actionplan')) { ?>
<hr />
<h3>Action Plan</h3>
<?php the_field('actionplan'); ?>
<?php } ?>

<hr />

<h3>Comparative Listings</h3>
<div class="alllists">
<?php the_field('mlslist'); ?>
</div>

</article>

<div id="sidebar" class="col span_3">
	<div id="sidebar-inner">
<div class="widget widget_ct_agentsotherlistings left">
<h5>Contact Info</h5>
Can be hard coded once received 
</div>
</div>

</div>

</div>

<?php get_footer(); ?>