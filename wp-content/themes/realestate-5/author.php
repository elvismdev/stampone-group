<?php
/**
 * Author/Agent
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

$layout = $ct_options['ct_layout'];

if(isset($_GET['author_name'])) :
	$curauth = get_user_by('slug', $author_name);
else :
	$curauth = get_userdata(intval($author));
endif;

$author_page_url = $curauth->user_url;
$profile_img = $curauth->ct_profile_url;

get_header();

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo __('Agent', 'contempo');
		echo '</h1>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<div class="container">';

		if($ct_options['ct_layout'] == 'left-sidebar') {
			echo '<div id="sidebar" class="col span_3">';
			if (function_exists('dynamic_sidebar') && dynamic_sidebar('Left Sidebar Agents') ) :else: endif;
			echo '</div>';
		} ?>

        <article class="col span_9 <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">
            
			<?php if($curauth->ct_profile_url) { ?>
                <figure class="col span_3">
                    <img class="author-img left" src="<?php echo aq_resize($profile_img,200); ?>" />
                </figure>
            <?php } ?>
             <div class="author-info col span_9">
                <h2 class="marB0"><a href="<?php echo $user_link; ?>" title="<?php echo $curauth->display_name; ?>"><?php echo $curauth->display_name; ?></a></h2>
                <?php if ($curauth->title) { ?><h3><?php echo $curauth->title; ?></h3><?php } ?>
                <?php if($curauth->tagline) { ?><p id="tagline"><strong><?php echo $curauth->tagline; ?></strong></p><?php } ?>
                <?php if($curauth->mobile) { ?><p class="marT10 marB3"><?php _e('Mobile', 'contempo'); ?>: <?php echo $curauth->mobile; ?></p><?php } ?>
                <?php if($curauth->office) { ?><p class="marB3"><?php _e('Office', 'contempo'); ?>: <?php echo $curauth->office; ?></p><?php } ?>
                <?php if($curauth->fax) { ?><p class="marB6"><?php _e('Fax', 'contempo'); ?>: <?php echo $curauth->fax; ?></p><?php } ?>

                <?php if($curauth->brokername) { ?><p class="marB3"><strong><?php echo $curauth->brokername; ?></strong></p><?php } ?>
                <?php if($curauth->brokernum) { ?><p class="marB3"><?php echo $curauth->brokernum; ?></p><?php } ?>
                
                <p><?php the_author_meta( 'description', $author->ID ); ?></p>
                
                <ul class="marL0">
                    <?php if ($author_twitter) { ?><li class="twitter"><a href="http://twitter.com/#!/<?php echo $author_twitter; ?>" target="_blank">Twitter</a></li><?php } ?>
                    <?php if ($author_facebook) { ?><li class="facebook"><a href="<?php echo $author_facebook; ?>" target="_blank"><?php _e( 'Facebook', 'contempo' ); ?></a></li><?php } ?>
                    <?php if ($author_linkedin) { ?><li class="facebook"><a href="<?php echo $author_linkedin; ?>" target="_blank"><?php _e( 'LinkedIn', 'contempo' ); ?></a></li><?php } ?>
                    <?php if ($author_google) { ?><li class="google"><a href="<?php echo $author_google; ?>" target="_blank"><?php _e( 'Google+', 'contempo' ); ?></a></li><?php } ?>
                </ul>
            </div>
            
            <div id="listings-results" class="<?php if($ct_options['ct_listings_layout'] == 'Grid') { echo 'grid'; } else { echo 'list'; } ?> <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">

				<?php
                    $ID = $curauth->ID;
                    query_posts( array(
                        'post_type' => 'listings',
						'posts_per_page' => 6,
                        'author' => $ID )
                    );
                    
						if($ct_options['ct_listings_layout'] == 'Grid') {
							get_template_part( 'layouts/grid');
						} else {
							get_template_part( 'layouts/list');
						}
                ?>
                
            </div>
            
            <?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'contempo' ) . '</span>', 'after' => '</div>' ) ); ?>
            
                <div class="clear"></div>

        </article>

		<?php if($ct_options['ct_layout'] == 'right-sidebar') {
			echo '<div id="sidebar" class="col span_3">';
			if (function_exists('dynamic_sidebar') && dynamic_sidebar('Right Sidebar Agents') ) :else: endif;
			echo '</div>';
		}

echo '</div>';

get_footer(); ?>