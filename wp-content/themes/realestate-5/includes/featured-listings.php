<?php
/**
 * Testimonials
 *
 * @package WP Pro Real Estate 5
 * @subpackage Include
 */
 
global $ct_options;
 
$feat_num = $ct_options['ct_home_featured_num'];

?>

<h3><span><?php _e('Featured Listings', 'contempo'); ?></span></h3>
<ul class="marT10">
    <?php 
        $args = array(
            'ct_status' => 'featured',
            'post_type' => 'listings',
            'posts_per_page' => $feat_num
        );
        query_posts($args);
        
        $count = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
        <li class="col span_3 <?php if($count % 4 == 0) { echo 'first'; } ?>">
            <figure>
                <?php ct_status(); ?>
                <a href="<?php the_permalink(); ?>"><?php ct_first_image_linked_view(); ?></a>
            </figure>
            <div class="featured-listing-info">
                <h4 class="marB0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <p class="location marB10"><?php city(); ?>, <?php state(); ?> <?php zipcode(); ?></p>
                 <?php if( (get_post_meta($post->ID, "_ct_price", true)) != "" ) { ?><p class="price marB10"><strong><?php currency(); ?><?php listing_price(); ?></strong><?php if(has_status('for-rent') || has_status('rental')) { echo " /month"; } ?></p><?php } ?>
                <p class="propinfo marB10">
					<?php                 
                        if(has_type('commercial')) { 
                            // Display Nothing 
                        } else {
                            beds(); baths();
                        }
                        
                        if((get_post_meta($post->ID, "_ct_sqft", true))) {
                            echo ' | ';
                            echo get_post_meta($post->ID, "_ct_sqft", true);
                            echo ' ';
                            sqftsqm();
                        }
                        
                        if((get_post_meta($post->ID, "_ct_lotsize", true))) {
                            if((get_post_meta($post->ID, "_ct_sqft", true))) {
                                echo ' | ';
                            }
                            echo get_post_meta($post->ID, "_ct_lotsize", true);
                            echo ' ';
                            acres();
                        }
                    ?>
                </p>
            </div>	
        </li>
        
        <?php
		
		$count++;
		
		if($count % 4 == 0) {
			echo '<div class="clear"></div>';
		}
		
		endwhile; endif; wp_reset_query(); ?>
</ul>
    <div class="clear"></div>