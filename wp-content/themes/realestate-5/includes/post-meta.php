<?php
/**
 * Post Meta
 *
 * @package WP Pro Real Estate 5
 * @subpackage Include
 */
 
$tags = get_the_tags();
 
 ?>

<div class="post-meta <?php if(is_single()) { echo 'marB40'; } ?>">
    <small class="marB0"><span style="padding:0 10px 0 0;margin:0 0 0 10px;"><i class="icon-user"></i> <?php the_author(); ?></span></small>
</div>