<?php
/**
 * Testimonials
 *
 * @package WP Pro Real Estate 5
 * @subpackage Include
 */
 
global $ct_options;

isset( $ct_options['ct_home_testimonial_num'] ) ? esc_attr( $ct_options['ct_home_testimonial_num'] ) : '';
isset( $ct_options['ct_home_testimonial_rand'] ) ? esc_attr( $ct_options['ct_home_testimonial_rand'] ) : '';

if(is_home()) {
	$test_num = $ct_options['ct_home_testimonial_num'];
} else {
	$test_num = -1;
}

$ct_orderby = $ct_options['ct_home_testimonial_rand'];
 
 ?>
<div class="testimonials">
	<?php
		$count = 0;
        $args = array(
            'post_type' => 'testimonial', 
            'order' => 'DSC',
			'orderby' => $ct_orderby,
            'posts_per_page' => $test_num
        );
        $query = new WP_Query($args); ?>
        
    <ul class="testimonial-home">
        
        <?php
		
		while ( $query->have_posts() ) : $query->the_post(); $count ++;
		
		$title = get_post_meta($post->ID, "_ct_person_title", true);
		$business = get_post_meta($post->ID, "_ct_business", true);
		$site_url = get_post_meta($post->ID, "_ct_site_url", true);
		
		?>
        
        <li class="col span_6 <?php if($count == 1 || $count == 3 || $count == 5 || $count == 7 || $count == 9) echo 'first'; ?>">
            <div class="test-content">
                <h3 class="marB20"><?php the_content(); ?></h3>
                <h5 class="marB0"><?php the_title(); ?></h5>
                <h6><?php echo $title; ?> &mdash; <a href="<?php echo $site_url; ?>"><?php echo $business; ?></a></h6>
            </div>
            <?php if(has_post_thumbnail()) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_size' );
				$url = $thumb['0'];
				echo '<figure class="marB5" style="background: url('.$url.') no-repeat center center;">';
				echo '</figure>';
			} ?>
        </li>
        
        <?php if(0 == $count%2) {
			echo '<div class="clear"></div>';
		} ?>
        
        <?php endwhile; wp_reset_query(); ?>
        
    </ul>
</div>