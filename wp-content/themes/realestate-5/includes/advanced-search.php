<?php
/**
 * Advanced Search Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */

global $ct_options;
$layout = $ct_options['ct_homepage_adv_search']['enabled'];

?>

<?php if(!is_archive()) { ?>

<section id="advanced-search" class="col span_12 first">
	<div class="container">
<div class="contained">

<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Homepage Search') ) :else: endif; ?>
</div>
            
                <div class="clear"></div>
            
        </form>

    </div>
</section>

<?php } ?>