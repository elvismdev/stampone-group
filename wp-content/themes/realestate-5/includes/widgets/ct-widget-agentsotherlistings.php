<?php
/**
 * Agents Other Listings
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */
 
class ct_AgentsOtherListings extends WP_Widget {

   function ct_AgentsOtherListings() {
	   $widget_ops = array('description' => 'Display your agents other listings, can only be used in the Listing Single sidebar as it relies on listing information to function properly.' );
	   parent::WP_Widget(false, __('CT Agents Other Listings', 'contempo'),$widget_ops);      
   }

   function widget($args, $instance) {  
	extract( $args );
	$title = $instance['title'];
	$number = $instance['number'];
	$taxonomy = $instance['taxonomy'];
	$tag = $instance['tag'];
	?>
		<?php echo $before_widget; ?>
		<?php if ($title) { echo $before_title . $title . $after_title; }
		echo '<ul>';
		global $post;
		$author = get_the_author_meta('ID');
		query_posts(array(
            'post_type' => 'listings', 
            'order' => 'DSC',
			$taxonomy => $tag,
			//'author' => $author,
            'posts_per_page' => $number
		));
            
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
            <li>
                <figure>
					<?php ct_status(); ?>
                    <?php ct_first_image_linked_view(); ?>
                </figure>
                <div class="listing-info">
                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <p class="location marB6"><?php city(); ?>, <?php state(); ?> <?php zipcode(); ?></p>
                    <?php if( (get_post_meta($post->ID, "_ct_price", true)) != "" ) { ?><p class="price"><?php if(has_status('for-rent')) { echo "Rental - "; } ?><strong><?php currency(); ?><?php listing_price(); ?></strong></p><?php } ?>
                    <p class="propinfo"><?php if(has_type('land') || has_type('lot')) { ?><?php echo get_post_meta($post->ID, "lotsize_value", true); ?> <?php acres(); ?><?php } elseif(has_type('commercial')) { ?><?php // Display Nothing ?><?php } else { ?><?php beds(); ?> <?php baths(); ?> | <?php echo get_post_meta($post->ID, "_ct_sqft", true);; ?> <?php sqftsqm(); ?><?php } ?></p>
                </div>
                    <div class="clear"></div>
            </li>

        <?php endwhile; endif; wp_reset_query(); ?>
		
		<?php echo '</ul>'; ?>
		
		<?php echo $after_widget; ?>   
    <?php
   }

   function update($new_instance, $old_instance) {                
	   return $new_instance;
   }

   function form($instance) {
	   
		$taxonomies = array (
			'property_type' => 'property_type',
			'beds' => 'beds',
			'baths' => 'baths',
			'status' => 'status',
			'city' => 'city',
			'state' => 'state',
			'zipcode' => 'zipcode',
			'additional_features' => 'additional_features'
		);
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? esc_attr( $instance['number'] ) : '';
		$taxonomy = isset( $instance['taxonomy'] ) ? esc_attr( $instance['taxonomy'] ) : '';
		$tag = isset( $instance['tag'] ) ? esc_attr( $instance['tag'] ) : '';
		
		?>
		<p>
		   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:','contempo'); ?></label>
            <select name="<?php echo $this->get_field_name('number'); ?>" class="widefat" id="<?php echo $this->get_field_id('number'); ?>">
                <?php for ( $i = 1; $i <= 10; $i += 1) { ?>
                <option value="<?php echo $i; ?>" <?php if($number == $i){ echo "selected='selected'";} ?>><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Taxonomy:','contempo'); ?></label>
            <select name="<?php echo $this->get_field_name('taxonomy'); ?>" class="widefat" id="<?php echo $this->get_field_id('taxonomy'); ?>">
                <?php
				foreach ($taxonomies as $tax => $value) { ?>
                <option value="<?php echo $tax; ?>" <?php if($taxonomy == $tax){ echo "selected='selected'";} ?>><?php echo $tax; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
		   <label for="<?php echo $this->get_field_id('tag'); ?>"><?php _e('Tag:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('tag'); ?>"  value="<?php echo $tag; ?>" class="widefat" id="<?php echo $this->get_field_id('tag'); ?>" />
		</p>
		<?php
	}
} 

register_widget('ct_AgentsOtherListings');
?>