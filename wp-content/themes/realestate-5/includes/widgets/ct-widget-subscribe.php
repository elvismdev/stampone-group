<?php
/**
 * Subscribe
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */
 
class ct_Subscribe extends WP_Widget {

   function ct_Subscribe() {
	   $widget_ops = array('description' => 'Subscribe form for sidebar.' );
	   parent::WP_Widget(false, __('CT Subscribe', 'contempo'),$widget_ops);      
   }

   function widget($args, $instance) {  
	extract( $args );
	?>
		<?php echo $before_widget; ?><!-- Begin MailChimp Signup Form -->
    <h5>Subscribe</h5>
<div id="mc_embed_signup">
<form action="http://stamponegroup.us7.list-manage2.com/subscribe/post?u=c39f76751528d6a38de9186ee&amp;id=34c395133b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="os">
<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="Email Address" onfocus="if (this.value == 'Email Address') {this.value=''}" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
</div>
<div class="ts">
<div class="mc-field-group">
	<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
</label>
	<input type="text" value="First Name" onfocus="if (this.value == 'First Name') {this.value=''}" name="FNAME" class="required" id="mce-FNAME">
</div>
</div>
<div style="clear:both;"></div>
<div class="os">
<div class="mc-field-group">
	<label for="mce-LNAME">Last Name </label>
	<input type="text" value="Last Name" onfocus="if (this.value == 'Last Name') {this.value=''}" name="LNAME" class="" id="mce-LNAME">
</div>
</div>
<div class="ts">
<div class="mc-field-group">
	<label for="mce-MMERGE3">Zip Code  <span class="asterisk">*</span>
</label>
	<input type="text" value="Zip Code" onfocus="if (this.value == 'Zip Code') {this.value=''}" name="MMERGE3" class="required" id="mce-MMERGE3">
</div>
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c39f76751528d6a38de9186ee_34c395133b" value=""></div>
	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
</form>
</div>

<!--End mc_embed_signup-->
</div>
</div>
</div>
		<?php
	}
} 

register_widget('ct_Subscribe');
?>