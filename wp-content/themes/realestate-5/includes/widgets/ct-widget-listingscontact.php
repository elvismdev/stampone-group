<?php
/**
 * Listings Contact Form
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */

class ct_ListingsContact extends WP_Widget {

   function ct_ListingsContact() {
	   $widget_ops = array('description' => 'Display an agent contact form. Can only be used in the Listing Single sidebar as it relies on listing information for content.' );
	   parent::WP_Widget(false, __('CT Listing Agent Contact', 'contempo'),$widget_ops);      
   }

   function widget($args, $instance) {  
	extract( $args );
	$title = $instance['title'];
	$subject = $instance['subject'];
	?>
		<?php echo $before_widget; ?>
		<?php if ($title) { echo $before_title . $title . $after_title; }
			global $ct_options;
		?>
        
            <form id="listingscontact" class="formular" method="post">
                <fieldset>
                    <input type="text" name="name" id="name" class="validate[required,custom[onlyLetter]] text-input" value="<?php _e('Name', 'contempo'); ?>" onfocus="if(this.value=='<?php _e('Name', 'contempo'); ?>')this.value = '';" onblur="if(this.value=='')this.value = '<?php _e('Name', 'contempo'); ?>';" />
                    
                    <input type="text" name="email" id="email" class="validate[required,custom[email]] text-input" value="<?php _e('Email', 'contempo'); ?>" onfocus="if(this.value=='<?php _e('Email', 'contempo'); ?>')this.value = '';" onblur="if(this.value=='')this.value = '<?php _e('Email', 'contempo'); ?>';" />
                    
                    <input type="text" name="ctphone" id="ctphone" class="text-input" value="<?php _e('Phone', 'contempo'); ?>" onfocus="if(this.value=='<?php _e('Phone', 'contempo'); ?>')this.value = '';" onblur="if(this.value=='')this.value = '<?php _e('Phone', 'contempo'); ?>';" />
                    
                    <textarea class="validate[required,length[2,500]] text-input" name="message" id="message" rows="5" cols="10"></textarea>
                    
                    <input type="hidden" id="ctyouremail" name="ctyouremail" value="<?php the_author_meta('user_email'); ?>" />
                    <input type="hidden" id="ctproperty" name="ctproperty" value="<?php the_title(); ?>, <?php city(); ?>, <?php state(); ?> <?php zipcode(); ?>" />
                    <input type="hidden" id="ctpermalink" name="ctpermalink" value="<?php the_permalink(); ?>" />
                    <input type="hidden" id="ctsubject" name="ctsubject" value="<?php echo $subject; ?>" />
                    
                    <input type="submit" name="Submit" value="<?php _e('Submit', 'contempo'); ?>" id="submit" class="btn" />  
                </fieldset>
            </form>
		
		<?php echo $after_widget; ?>   
    <?php
   }

   function update($new_instance, $old_instance) {                
	   return $new_instance;
   }

   function form($instance) {
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$subject = isset( $instance['subject'] ) ? esc_attr( $instance['subject'] ) : '';
		
		?>
		<p>
		   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
        <p>
		   <label for="<?php echo $this->get_field_id('subject'); ?>"><?php _e('Subject:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('subject'); ?>"  value="<?php echo $subject; ?>" class="widefat" id="<?php echo $this->get_field_id('subject'); ?>" />
		</p>
		<?php
	}
} 

register_widget('ct_ListingsContact');
?>