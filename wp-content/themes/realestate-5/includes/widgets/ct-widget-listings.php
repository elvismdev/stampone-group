<?php
/**
 * Listings
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */
 
class ct_Listings extends WP_Widget {

   function ct_Listings() {
	   $widget_ops = array('description' => 'Display your latest listings.' );
	   parent::WP_Widget(false, __('CT Listings', 'contempo'),$widget_ops);      
   }

   function widget($args, $instance) {  
	extract( $args );
	$title = $instance['title'];
	$number = $instance['number'];
	$taxonomy = $instance['taxonomy'];
	$tag = $instance['tag'];
	$viewalltext = $instance['viewalltext'];
	$viewalllink = $instance['viewalllink'];
	?>
		<?php echo $before_widget; ?>
		<?php if ($title) { echo $before_title . $title . $after_title; }
		echo '<ul>';
		global $post;
		query_posts(array(
            'post_type' => 'listings', 
            'order' => 'DSC',
			$taxonomy => $tag,
            'posts_per_page' => $number
		));
            
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
            <li>
                <figure>
					<?php ct_status(); ?>
                    <?php ct_first_image_linked_view(); ?>
                </figure>
                <div class="listing-info">
                    <h4 class="marB0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <p class="location marB10"><?php city(); ?>, <?php state(); ?> <?php zipcode(); ?></p>
                    <?php if( (get_post_meta($post->ID, "_ct_price", true)) != "" ) { ?><p class="price marB10"><strong><?php currency(); ?><?php listing_price(); ?></strong><?php if(has_status('for-rent') || has_status('rental')) { echo " /month"; } ?></p><?php } ?>
                    <p class="propinfo marB10">
						<?php                 
                            if(has_type('commercial')) { 
                                // Display Nothing 
                            } else {
                                beds(); baths();
                            }
                            
                            if((get_post_meta($post->ID, "_ct_sqft", true))) {
                                echo ' | ';
                                echo get_post_meta($post->ID, "_ct_sqft", true);
                                echo ' ';
                                sqftsqm();
                            }
							
							echo get_post_meta($post->ID, "_ct_lotsize", true);
                            
                            if((get_post_meta($post->ID, "_ct_lotsize", true))) {
                                if((get_post_meta($post->ID, "_ct_sqft", true))) {
                                    echo ' | ';
                                }
                                echo get_post_meta($post->ID, "_ct_lotsize", true);
                                echo ' ';
                                acres();
                            }
                        ?>
                    </p>
                </div>
                    <div class="clear"></div>
            </li>

        <?php endwhile; endif; wp_reset_query(); ?>
		
		<?php echo '</ul>'; ?>
        
           <?php if($viewalltext) { ?>
               <p id="viewall"><a class="read-more right" href="<?php echo $viewalllink; ?>"><?php echo $viewalltext; ?> <em>&rarr;</em></a></p>
           <?php } ?>
		
		<?php echo $after_widget; ?>   
    <?php
   }

   function update($new_instance, $old_instance) {                
	   return $new_instance;
   }

   function form($instance) {
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? esc_attr( $instance['number'] ) : '';
		$taxonomy = isset( $instance['taxonomy'] ) ? esc_attr( $instance['taxonomy'] ) : '';
		$tag = isset( $instance['tag'] ) ? esc_attr( $instance['tag'] ) : '';
		$viewalltext = isset( $instance['viewalltext'] ) ? esc_attr( $instance['viewalltext'] ) : '';
		$viewalllink = isset( $instance['viewalllink'] ) ? esc_attr( $instance['viewalllink'] ) : '';
		
		?>
		<p>
		   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:','contempo'); ?></label>
            <select name="<?php echo $this->get_field_name('number'); ?>" class="widefat" id="<?php echo $this->get_field_id('number'); ?>">
                <?php for ( $i = 1; $i <= 10; $i += 1) { ?>
                <option value="<?php echo $i; ?>" <?php if($number == $i){ echo "selected='selected'";} ?>><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('taxonomy'); ?>"><?php _e('Taxonomy:','contempo'); ?></label>
            <select name="<?php echo $this->get_field_name('taxonomy'); ?>" class="widefat" id="<?php echo $this->get_field_id('taxonomy'); ?>">
                <?php
				foreach (get_object_taxonomies( 'listings', 'objects' ) as $tax => $value) { ?>
                <option value="<?php echo $tax; ?>" <?php if($taxonomy == $tax){ echo "selected='selected'";} ?>><?php echo $tax; ?></option>
                <?php } ?>
            </select>
        </p>
        <p>
		   <label for="<?php echo $this->get_field_id('tag'); ?>"><?php _e('Tag:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('tag'); ?>"  value="<?php echo $tag; ?>" class="widefat" id="<?php echo $this->get_field_id('tag'); ?>" />
		</p>
        <p>
		   <label for="<?php echo $this->get_field_id('viewalltext'); ?>"><?php _e('View All Link Text','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('viewalltext'); ?>"  value="<?php echo $viewalltext; ?>" class="widefat" id="<?php echo $this->get_field_id('viewalltext'); ?>" />
		</p>
        <p>
		   <label for="<?php echo $this->get_field_id('viewalllink'); ?>"><?php _e('View All Link URL','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('viewalllink'); ?>"  value="<?php echo $viewalllink; ?>" class="widefat" id="<?php echo $this->get_field_id('viewalllink'); ?>" />
		</p>
		<?php
	}
} 

register_widget('ct_Listings');
?>