<?php
/**
 * Listings Search
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */
 
class ct_ListingsSearch extends WP_Widget {

   function ct_ListingsSearch() {
	   $widget_ops = array('description' => 'Display the listings search.' );
	   parent::WP_Widget(false, __('CT Listings Search', 'contempo'),$widget_ops);      
   }

   function widget($args, $instance) {  
	extract( $args );
	
	$title = $instance['title'];
	
	?>
		<?php echo $before_widget; ?>
		<?php if ($title) { echo $before_title . $title . $after_title; } ?>
        
        <form id="advanced_search" name="search-listings" action="<?php echo home_url(); ?>">

            <label for="ct_type"><?php _e('Type', 'contempo'); ?></label>
            <?php ct_search_form_select('property_type'); ?>

            <div id="ct_city" class="left marR10">
                <label for="ct_city"><?php _e('City', 'contempo'); ?></label>
                <?php ct_search_form_select('city'); ?>
            </div>

            <div id="ct_state" class="left">
                <label for="ct_state"><?php _e('State', 'contempo'); ?></label>
                <?php ct_search_form_select('state'); ?>
            </div>

            <div id="ct_zipcode" class="left marR0 clear">
                <label for="ct_zipcode"><?php _e('Zipcode', 'contempo'); ?></label>
                <?php ct_search_form_select('zipcode'); ?>
            </div>
            
                <div class="clear"></div>
            
            <div id="ct_beds" class="left marR10">
                <label for="ct_beds"><?php _e('Beds', 'contempo'); ?></label>
                <?php ct_search_form_select('beds'); ?>
            </div>
            
            <div id="ct_baths" class="left marR10">
                <label for="ct_baths"><?php _e('Baths', 'contempo'); ?></label>
                <?php ct_search_form_select('baths'); ?>
            </div>
            
            <div id="ct_status" class="left marR0">
                <label for="ct_status"><?php _e('Status', 'contempo'); ?></label>
                <?php ct_search_form_select('ct_status'); ?>
            </div>
            
                <div class="clear"></div>

           <?php /* <div id="ct_additional_features" class="left clear">
                <label for="ct_additional_features"><?php _e('Addtional Features', 'contempo'); ?></label>
				<?php ct_search_form_select('additional_features'); ?>
            </div>
			*/ ?>

			<?php global $wp_version; if (version_compare($wp_version, '3.1', '>=')) : ?>
                <div id="ct_price_from" class="left marR10">
                    <label for="ct_price_from"><?php _e('Price From ($)', 'contempo'); ?></label>
                    <input type="text" id="ct_price_from" class="number" name="ct_price_from" size="8" />
                </div>
                <div id="ct_price_to" class="left marR10">
                    <label for="ct_price_to"><?php _e('Price To ($)', 'contempo'); ?></label>
                    <input type="text" id="ct_price_to" class="number" name="ct_price_to" size="8" />
                </div>
            <?php endif; ?>

            <div id="ct_mls" class="left">
                <label for="ct_mls"><?php _e('MLS #', 'contempo'); ?></label>
                <input type="text" id="ct_mls" name="ct_mls" size="12" />
            </div>
            
                <div class="clear"></div>
            
            <input type="hidden" name="search-listings" value="true" />
            <input id="submit" class="btn marB0" type="submit" value="<?php _e('Search', 'contempo'); ?>" />

        </form>
		
		<?php echo $after_widget; ?>   
    <?php
   }

   function update($new_instance, $old_instance) {                
	   return $new_instance;
   }

   function form($instance) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';

		?>
		<p>
		   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<?php
	}
} 

register_widget('ct_ListingsSearch');
?>