<?php
/**
 * Agent Info
 *
 * @package WP Pro Real Estate 5
 * @subpackage Widget
 */
 
class ct_AgentInfo extends WP_Widget {

   function ct_AgentInfo() {
	   $widget_ops = array('description' => 'Use this widget to display your listing agent information, can only be used in the Listing Single sidebar as it relies on listing information for content.' );
	   parent::WP_Widget(false, __('CT Agent Info', 'contempo'),$widget_ops);    
   }

   function widget($args, $instance) {  
	extract( $args );
	
	$title = $instance['title'];
	
	?>
		<?php
		
        echo $before_widget;
		
		if ($title) {
			echo $before_title . $title . $after_title;
		}        
        
		if(get_the_author_meta('ct_profile_url')) {
			echo '<figure class="col span_3">';
			echo '<a href="';
			echo site_url() . '/?author=';
			echo the_author_meta('ID');
			echo '">';
			echo '<img class="authorimg" src="';
			echo aq_resize(the_author_meta('ct_profile_url'),120);
			echo '" />';
			echo '</a>';
			echo '</figure>';
		} ?>
        
        <div class="details col span_8">
            <h4 class="author marB5"><a href="<?php echo home_url(); ?>/?author=<?php the_author_meta('ID'); ?>"><?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?></a></h4>
            <?php if(get_the_author_meta('tagline')) { ?><p class="tagline marB0"><strong><?php the_author_meta('tagline'); ?></strong></p><?php } ?>
            <?php if(get_the_author_meta('mobile')) { ?><p class="marT3 marB0"><?php _e('Mobile', 'contempo'); ?>: <?php the_author_meta('mobile'); ?></p><?php } ?>
            <?php if(get_the_author_meta('office')) { ?><p class="marT3 marB0"><?php _e('Office', 'contempo'); ?>: <?php the_author_meta('office'); ?></p><?php } ?>
            <?php if(get_the_author_meta('fax')) { ?><p class="marT3 marB0"><?php _e('Fax', 'contempo'); ?>: <?php the_author_meta('fax'); ?></p><?php } ?>
        </div>		
		<?php echo $after_widget; ?>   
    <?php
   }

   function update($new_instance, $old_instance) {                
	   return $new_instance;
   }

   function form($instance) { 
		
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		
		?>
		<p>
		   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','contempo'); ?></label>
		   <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
	<?php }
} 

register_widget('ct_AgentInfo');
?>