<?php
/**
 * Template Name: Left Sidebar
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options; 

get_header();

echo '<header id="archive-header" class="marB40">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo the_title();
		echo '</h1>';
		echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

echo '<div class="container">';

		echo '<div id="sidebar" class="col span_3 first">';
			echo '<div id="sidebar-inner">';
				if (function_exists('dynamic_sidebar') && dynamic_sidebar('Left Sidebar Pages') ) :else: endif;
			echo '</div>';
		echo '</div>';

		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article class="col span_9">
            
			<?php the_content(); ?>
            
            <?php //wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'contempo' ) . '</span>', 'after' => '</div>' ) ); ?>
            
            <?php endwhile; endif; ?>
            
                <div class="clear"></div>

        </article>

<?php 

echo '</div>';

get_footer(); ?>