<?php
/**
 * Sidebar Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */
 
global $ct_options;

?>

<div id="sidebar" class="col span_3 <?php if($ct_options['ct_layout'] == 'left-sidebar') { echo 'first'; } ?>">
	<div id="sidebar-inner">
           <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Listing Single Right') ) :else: endif; ?>
		<div class="clear"></div>
	</div>
</div>