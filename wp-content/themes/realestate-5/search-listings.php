<?php
/**
 * Search Listings Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */

$layout = $ct_options['ct_layout'];

/*-----------------------------------------------------------------------------------*/
/* Query multiple taxonomies */
/*-----------------------------------------------------------------------------------*/

$taxonomies_to_search = array(
	'beds' => 'Bedrooms',
	'baths' => 'Bathrooms',
	'property_type' => 'Property Type',
	'ct_status' => 'Status',
	'state' => 'State',
	'zipcode' => 'Zipcode',
	'city' => 'City',
	'additional_features' => 'Additional Features'
);                                                                       

$search_values = array();

foreach ($taxonomies_to_search as $t => $l) {
	$var_name = 'ct_'. $t;
	
	if (!empty($_GET[$var_name])) {  
		$search_values[$t] = utf8_encode($_GET[$var_name]);
	}                                                     
}                                                          
                                   
$search_values['post_type'] = 'listings';
$search_values['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;

global $ct_options;

$search_num = $ct_options['ct_listing_search_num'];
$search_values['showposts'] = $search_num;

$mode = 'search'; 
    
// Check Price From/To
if (!empty($_GET['ct_price_from']) && !empty($_GET['ct_price_to'])) {
	$ct_price_from = str_replace(',', '', $_GET['ct_price_from']);
	$ct_price_to = str_replace(',', '', $_GET['ct_price_to']);
	$search_values['meta_query'] = array(
		array(
			'key' => '_ct_price',
			'value' => array( $ct_price_from, $ct_price_to ),
			'type' => 'numeric',
			'compare' => 'BETWEEN'
		)
	);
}
else if (!empty($_GET['ct_price_from'])) {               
	$ct_price_from = str_replace(',', '', $_GET['ct_price_from']);
	$search_values['meta_query'] = array(
		array(
			'key' => '_ct_price',
			'value' => $ct_price_from,
			'type' => 'numeric',
			'compare' => '>='
		)
	);	
}
else if (!empty($_GET['ct_price_to'])) {               
	$ct_price_to = str_replace(',', '', $_GET['ct_price_to']);
	$search_values['meta_query'] = array(
		array(
			'key' => '_ct_price',
			'value' => $ct_price_to,
			'type' => 'numeric',
			'compare' => '<='
		)
	);	
}

/*-----------------------------------------------------------------------------------*/
/* Check to see if reference number matches */
/*-----------------------------------------------------------------------------------*/ 
 
if (!empty($_GET['ct_mls'])) {
	$ct_mls = $_GET['ct_mls'];
	$search_values['meta_query'] = array(
		array(
			'key' => '_ct_mls',
			'value' => $ct_mls,
			'type' => 'numeric',
			'compare' => '='
		)
	);	
}

global $wp_query;

/*-----------------------------------------------------------------------------------*/
/* Save the existing query */
/*-----------------------------------------------------------------------------------*/ 

$existing_query_obj = $wp_query;

$wp_query = new WP_Query( $search_values ); 
$total_results = $wp_query->found_posts;
unset($search_values['post_type']);
unset($search_values['paged']);
unset($search_values['showposts']);   

/*-----------------------------------------------------------------------------------*/
/* Prepare the title string by looping through
all the values we're going to query and put them together */
/*-----------------------------------------------------------------------------------*/                             

$search_params = ''; 
$loop = 0;
foreach ($search_values as $t => $s) {                                     
	if ($loop == 1) { 
		$search_params .= ', '; 
	} else {
		$loop = 1;
	}                              
	
	$term = get_term_by('slug',$s,$t);
	$name = $term->name;   
	
	$search_params .= '<strong>'. $name .'</strong>';                                    
}

get_header();

    echo '<header id="archive-header" class="marB40">';
        echo '<div class="container">';
            echo '<h1 class="marB0 left">';
			echo $total_results;
			echo ' ';
			if($total_results != '1') { _e('listings found', 'contempo'); } else { _e('listing found', 'contempo'); }
			echo '</h1>';
			echo ct_breadcrumbs();
		echo '<div class="clear"></div>';
        echo '</div>';
    echo '</header>';
	
	echo '<div class="container">';
	
		echo '<h5>';
		if($search_params !="") { echo __('Searching: ', 'contempo'); echo $search_params; } else { echo __('Searching all listings', 'contempo'); }
		echo '</h5>';
	
		// Start Search Results Map
		if($ct_options['ct_disable_google_maps'] == 'No') {
			echo '<div id="map-wrap">';
			 
				wp_reset_postdata();
				
				$search_values['post_type'] = 'listings';
				$search_values['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$search_values['showposts'] = $search_num;
				$wp_query = new wp_query( $search_values ); 
		
					search_results_map();
			
			// End Search Results Map
			echo '</div>';
		}
		
		// Left Sidebar
		if($layout == 'left-sidebar') {
			echo '<div id="sidebar" class="col span_3 first">';
			if (function_exists('dynamic_sidebar') && dynamic_sidebar('Listings Search Left') ) :else: endif;
			echo '</div>';
		} ?>
		
			<div id="listings-results" class="col span_9 <?php if($ct_options['ct_listings_layout'] == 'Grid') { echo 'grid'; } else { echo 'list'; } ?> <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">

				<?php
                
					// Reset Query for Listings
					wp_reset_query();
					wp_reset_postdata();
				
					$search_values['post_type'] = 'listings';
					$search_values['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$search_values['showposts'] = $search_num;
					$wp_query = new wp_query( $search_values ); 
					
					if($ct_options['ct_listings_layout'] == 'Grid') {
						get_template_part( 'layouts/grid');
					} else {
						get_template_part( 'layouts/list');
					}
				
			// End Search Results Map
			echo '</div>';

		// Restore WP_Query object
		$wp_query = $existing_query_obj;
		
		// Right Sidebar
		if($layout == 'right-sidebar') {
			echo '<div id="sidebar" class="col span_3">';
			if (function_exists('dynamic_sidebar') && dynamic_sidebar('Listings Search Right') ) :else: endif;
			echo '</div>';
		}
		
	echo '</div>';

get_footer(); ?>