<?php
/**
 * Archive Template
 *
 * @package WP Pro Real Estate 5
 * @subpackage Template
 */

get_header();

echo '<header id="archive-header">';
	echo '<div class="container">';
		echo '<h1 class="marB0 left">';
		echo 'Featured Listings';
		echo '</h1>';
		echo '<div class="clear"></div>';
	echo '</div>';
echo '</header>';

get_template_part('/includes/advanced-search');

echo '<section class="container">';

		if($ct_options['ct_layout'] == 'left-sidebar') {
			get_sidebar();
		} ?>

        <div class="col span_9 <?php if($ct_options['ct_layout'] == 'right-sidebar') { echo 'first'; } ?>">
        
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
                <?php get_template_part( 'content', get_post_format() ); ?>
                
            <?php endwhile; ?>
            
                <?php ct_pagination(); ?>
            
            <?php else : ?>
            
                <?php get_template_part( 'content', 'none' ); ?>
            
            <?php endif; ?>

        </div>
        
        <?php if($ct_options['ct_layout'] == 'right-sidebar') {
			get_sidebar();
		}
        
echo '</section>';

get_footer(); ?>