<?php
/**
 * Register Custom Taxonomies
 *
 * @package WP Pro Real Estate 5
 * @subpackage Admin
 */

// Property Type
$ptlabels = array(
	'name' => _x( 'Property Type', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Property Type', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Property Types', 'contempo' ),
	'popular_items' => __( 'Popular Property Types', 'contempo' ),
	'all_items' => __( 'All Property Types', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Property Type', 'contempo' ),
	'update_item' => __( 'Update Property Type', 'contempo' ),
	'add_new_item' => __( 'Add New Property Type', 'contempo' ),
	'new_item_name' => __( 'New Property Type Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Property Types with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Property Types', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Property Types', 'contempo' )
);
register_taxonomy( 'property_type', 'listings', array(
	'hierarchical' => false,
	'labels' => $ptlabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'type' ),
));

function propertytype() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'property_type', '', ', ', '' ) );
	echo $terms_as_text;
}

// Beds
$bedslabels = array(
	'name' => _x( 'Beds', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Beds', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Beds', 'contempo' ),
	'popular_items' => __( 'Popular Beds', 'contempo' ),
	'all_items' => __( 'All Beds', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Beds', 'contempo' ),
	'update_item' => __( 'Update Beds', 'contempo' ),
	'add_new_item' => __( 'Add New Beds', 'contempo' ),
	'new_item_name' => __( 'New Beds Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Beds with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Beds', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Beds', 'contempo' )
);
register_taxonomy( 'beds', 'listings', array(
	'hierarchical' => false,
	'labels' => $bedslabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'beds' ),
));

function beds() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'beds', '', ', ', '' ) );
	if($terms_as_text != '') {
		echo $terms_as_text; _e(' Bed', 'contempo');
	}
}

// Baths
$bathslabels = array(
	'name' => _x( 'Baths', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Baths', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Baths', 'contempo' ),
	'popular_items' => __( 'Popular Baths', 'contempo' ),
	'all_items' => __( 'All Baths', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Baths', 'contempo' ),
	'update_item' => __( 'Update Baths', 'contempo' ),
	'add_new_item' => __( 'Add New Baths', 'contempo' ),
	'new_item_name' => __( 'New Baths Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Baths with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Baths', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Baths', 'contempo' )
);
register_taxonomy( 'baths', 'listings', array(
	'hierarchical' => false,
	'labels' => $bathslabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'baths' ),
));

function baths() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'baths', '', ', ', '' ) );
	if($terms_as_text != '') {
		echo ' | '; echo $terms_as_text; _e(' Bath', 'contempo');
	}
}

// Status
$statuslabels = array(
	'name' => _x( 'Status', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Status', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Statuses', 'contempo' ),
	'popular_items' => __( 'Popular Statuses', 'contempo' ),
	'all_items' => __( 'All Statuses', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Statuses', 'contempo' ),
	'update_item' => __( 'Update Statuses', 'contempo' ),
	'add_new_item' => __( 'Add New Status', 'contempo' ),
	'new_item_name' => __( 'New Status Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Statuses with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Status', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Statuses', 'contempo' )
);
register_taxonomy( 'ct_status', 'listings', array(
	'hierarchical' => false,
	'labels' => $statuslabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'status' ),
));

function status() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'ct_status', '', ', ', '' ) );
	echo $terms_as_text;
}

// City
$citylabels = array(
	'name' => _x( 'City', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'City', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Cities', 'contempo' ),
	'popular_items' => __( 'Popular Cities', 'contempo' ),
	'all_items' => __( 'All Cities', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Cities', 'contempo' ),
	'update_item' => __( 'Update City', 'contempo' ),
	'add_new_item' => __( 'Add New City', 'contempo' ),
	'new_item_name' => __( 'New City Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Cities with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Cities', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Cities', 'contempo' )
);
register_taxonomy( 'city', 'listings', array(
	'hierarchical' => false,
	'labels' => $citylabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'city' ),
));

function city() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'city', '', ', ', '' ) );
	echo $terms_as_text;
}

// State
$statelabels = array(
	'name' => _x( 'State', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'State', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search States', 'contempo' ),
	'popular_items' => __( 'Popular States', 'contempo' ),
	'all_items' => __( 'All States', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit States', 'contempo' ),
	'update_item' => __( 'Update State', 'contempo' ),
	'add_new_item' => __( 'Add New State', 'contempo' ),
	'new_item_name' => __( 'New State Name', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate States with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove States', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used States', 'contempo' )
);
register_taxonomy( 'state', 'listings', array(
	'hierarchical' => false,
	'labels' => $statelabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'state' ),
));

function state() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'state', '', ', ', '' ) );
	echo $terms_as_text;
}

// Zipcode
$ziplabels = array(
	'name' => _x( 'Zipcode', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Zipcode', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Zipcodes', 'contempo' ),
	'popular_items' => __( 'Popular Zipcodes', 'contempo' ),
	'all_items' => __( 'All Zipcodes', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Zipcode', 'contempo' ),
	'update_item' => __( 'Update Zipcode', 'contempo' ),
	'add_new_item' => __( 'Add New Zipcode', 'contempo' ),
	'new_item_name' => __( 'New Zipcode', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Zipcodes with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Zipcodes', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Zipcodes', 'contempo' )
);
register_taxonomy( 'zipcode', 'listings', array(
	'hierarchical' => false,
	'labels' => $ziplabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'zipcode' ),
));

function zipcode() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'zipcode', '', ', ', '' ) );
	echo $terms_as_text;
}

// Additional Features
$addfeatlabels = array(
	'name' => _x( 'Additional Features', 'taxonomy general name', 'contempo' ),
	'singular_name' => _x( 'Additional Feature', 'taxonomy singular name', 'contempo' ),
	'search_items' =>  __( 'Search Additional Features', 'contempo' ),
	'popular_items' => __( 'Popular Additional Features', 'contempo' ),
	'all_items' => __( 'All Additional Features', 'contempo' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Edit Additional Features', 'contempo' ),
	'update_item' => __( 'Update Additional Feature', 'contempo' ),
	'add_new_item' => __( 'Add New Additional Feature', 'contempo' ),
	'new_item_name' => __( 'New Additional Feature', 'contempo' ),
	'separate_items_with_commas' => __( 'Separate Additional Features with commas', 'contempo' ),
	'add_or_remove_items' => __( 'Add or remove Additional Features', 'contempo' ),
	'choose_from_most_used' => __( 'Choose from the most used Additional Features', 'contempo' )
);
register_taxonomy( 'additional_features', 'listings', array(
	'hierarchical' => false,
	'labels' => $addfeatlabels,
	'show_ui' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'features' ),
));

function addfeat() {
	global $post;
	global $wp_query;
	$terms_as_text = strip_tags( get_the_term_list( $wp_query->post->ID, 'additional_features', '', ', ', '' ) );
	echo $terms_as_text;
}

function addfeatlist () {
	global $post;
	$terms = get_the_terms($post->ID, 'additional_features');
	if ($terms) {
		echo '<h4 class="border-bottom marB20">' . __('Property Features', 'contempo') . '</h4>';
		echo '<ul class="propfeatures left marR30">';
		$count = 0;
		foreach ($terms as $taxindex => $taxitem) {
			echo '<li>' . $taxitem->name . '</li>';
			$count++;
			if ($count == 6) {
				echo '</ul><ul class="propfeatures left marR20">';
				$count = 0;
			}
		}
		echo '</ul>';
		echo '<div class="clear"></div>';
	} else {
		
	}
}

?>