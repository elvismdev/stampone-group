<?php
// Include & setup custom metabox and fields
$prefix = '_ct_'; // start with an underscore to hide fields from custom fields list
add_filter( 'cmb_meta_boxes', 'ct_metaboxes' );
function ct_metaboxes( $meta_boxes ) {
	global $prefix;
	$meta_boxes[] = array(
		'id' => 'listing_info',
		'title' => 'Listing Info',
		'pages' => array('listings'), // post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => false, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Price',
				'desc' => 'Enter the price here, without commas or seperators.',
				'id' => $prefix . 'price',
				'type' => 'text_money'
			),
			array(
				'name' => 'Sq Ft',
				'desc' => 'Enter the square footage here.',
				'id' => $prefix . 'sqft',
				'type' => 'text'
			),
			array(
				'name' => 'Lot Size',
				'desc' => 'Enter the lot size here.',
				'id' => $prefix . 'lotsize',
				'type' => 'text'
			),
			array(
				'name' => 'MLS #',
				'desc' => 'Enter the MLS # here.',
				'id' => $prefix . 'mls',
				'type' => 'text'
			),
			array(
				'name' => 'Broker',
				'desc' => 'Enter the Broker here.',
				'id' => $prefix . 'broker',
				'type' => 'text'
			),
			array(
				'name' => 'Latitude &amp; Longitude',
				'desc' => '<strong>OPTIONAL:</strong> Only use the latitude and longitude if the regular full address can\'t be found. (ex: 37.4419, -122.1419)',
				'id' => $prefix . 'latlng',
				'type' => 'text'
			),
		)
	);

	$meta_boxes[] = array(
		'id' => 'video_metabox',
		'title' => 'Video',
		'pages' => array('post','listings'), // post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => false, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Video',
				'desc' => 'Paste your video embed code here, size will automatically be adjusted to fit.',
				'id' => $prefix . 'video',
				'type' => 'textarea_code'
			),
		)
	);
	
	return $meta_boxes;
}

// Initialize the metabox class
add_action( 'init', 'be_initialize_cmb_meta_boxes', 9999 );
function be_initialize_cmb_meta_boxes() {
	if ( !class_exists( 'cmb_Meta_Box' ) ) {
		require_once (ADMIN_PATH . 'metabox/init.php');
	}
}