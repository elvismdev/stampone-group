# Translation of the WordPress plugin Query Multiple Taxonomies 1.3-alpha8 by scribu.
# Copyright (C) 2010 scribu
# This file is distributed under the same license as the Query Multiple Taxonomies package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Query Multiple Taxonomies 1.3-alpha8\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tag/query-multiple-taxonomies\n"
"POT-Creation-Date: 2010-08-11 17:37+0300\n"
"PO-Revision-Date: 2010-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: scb/AdminPage.php:167
msgid "Settings <strong>saved</strong>."
msgstr ""

#: scb/AdminPage.php:179 scb/AdminPage.php:189
msgid "Save Changes"
msgstr ""

#: scb/AdminPage.php:371
msgid "Settings"
msgstr ""

#: widget.php:42
msgid "Title:"
msgstr ""

#: widget.php:53
msgid "lists"
msgstr ""

#: widget.php:54
msgid "dropdowns"
msgstr ""

#: widget.php:57
msgid "Mode:"
msgstr ""

#: widget.php:62
msgid "Taxonomies:"
msgstr ""

#: widget.php:72
#, php-format
msgid "Post type: %s"
msgid_plural "Post types: %s"
msgstr[0] ""
msgstr[1] ""

#: widget.php:92
msgid "No taxonomies selected!"
msgstr ""

#: widget.php:126
msgid "Reset"
msgstr ""

#: widget.php:143
msgid "Remove all terms in group"
msgstr ""

#: widget.php:214
msgid "Add term"
msgstr ""

#: widget.php:222
msgid "Remove term"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Query Multiple Taxonomies"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://scribu.net/wordpress/query-multiple-taxonomies/"
msgstr ""

#. Description of the plugin/theme
msgid "Filter posts through multiple custom taxonomies"
msgstr ""

#. Author of the plugin/theme
msgid "scribu"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://scribu.net"
msgstr ""
