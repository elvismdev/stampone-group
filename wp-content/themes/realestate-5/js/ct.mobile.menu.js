jQuery(function($){
	$(document).ready(function(){

		$("<select />").appendTo("header nav");
		$("<option />", {
		   "selected": "selected",
		   "value" : "",
		   "text" : "Go To..."
		}).appendTo("header nav select");

		$("header nav a").each(function() {
			var el = $(this);
			if(el.parents('.sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr("href"),
				 'text' : '- ' + el.text()
				}).appendTo("header nav select");
			}
			else if(el.parents('.sub-menu .sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : '-- ' + el.text()
				}).appendTo("header nav select");
			}
			else {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : el.text()
				}).appendTo("header nav select");
			}
		});

		$("header nav select").change(function() {
		  window.location = $(this).find("option:selected").val();
		});
	
	});
});