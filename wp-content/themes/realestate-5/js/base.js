/**
 * Core
 *
 * @package WP Pro Real Estate 5
 * @subpackage JavaScript
 */

jQuery.noConflict();

jQuery(document).ready(function($){
	
	/*-----------------------------------------------------------------------------------*/
	/* Twitter Footer */
	/*-----------------------------------------------------------------------------------*/
	
	$('#twitter-feed ul').cycle({
		fx:	'fade'
	});
	
	/*-----------------------------------------------------------------------------------*/
	/* Testimonials Widget */
	/*-----------------------------------------------------------------------------------*/
	
	$('.widget_ct_testimonials .testimonials').cycle({ 
		fx:     'fade', 
		speed:  'fast', 
		timeout: 0, 
		next:   '.next.test', 
		prev:   '.prev.test' 
	});
	
	/*-----------------------------------------------------------------------------------*/
	/* Symple Skillbar Shortcode */
	/*-----------------------------------------------------------------------------------*/
	
	$('.symple-skillbar').each(function(){
		$(this).find('.symple-skillbar-bar').animate({ width: $(this).attr('data-percent') }, 1500 );
	});
	
	/*-----------------------------------------------------------------------------------*/
	/* Topbar Search Animation */
	/*-----------------------------------------------------------------------------------*/
	
	$('header a.search').click(function(e){
		e.preventDefault();
		
		$(this).hide();
		
		$('header form.searchform, input.s').show().animate({width:'140px'},300,   function(){
			$('header input.s').focus();
		});
	
	});
	
	$('header input.s').blur(function() {
		$('header form.searchform, input.s').hide().animate({width:'0'},300);
		$('header a.search').show().fadein('fast');
	});
	
	/*-----------------------------------------------------------------------------------*/
	/* Initialize FitVids */
	/*-----------------------------------------------------------------------------------*/
	
	$(".container").fitVids();
	
	/*-----------------------------------------------------------------------------------*/
	/* Add class for prev/next icons */
	/*-----------------------------------------------------------------------------------*/
	
	$('.prev-next .nav-prev a').addClass('icon-arrow-left');
	$('.prev-next .nav-next a').addClass('icon-arrow-right');
	
	$('#archive header p').addClass('marB0 right');
	
	/*-----------------------------------------------------------------------------------*/
	/* Add Font Awesome Icon to Sitemap list */
	/*-----------------------------------------------------------------------------------*/
	
	$('.page-template-template-sitemap-php #main-content li a').before('<i class="icon-caret-right"></i>');
	
	/*-----------------------------------------------------------------------------------*/
	/* Add last class to every third related item, and every second testimonial */
	/*-----------------------------------------------------------------------------------*/
	
	$("li.related:nth-child(3n+3)").addClass("last");
	
	/*-----------------------------------------------------------------------------------*/
	/* Initialize PrettyPhoto */
	/*-----------------------------------------------------------------------------------*/
	
	$("a[rel^='prettyPhoto']").prettyPhoto();
	
	/*-----------------------------------------------------------------------------------*/
	/* Add drop class to sub-menus */
	/*-----------------------------------------------------------------------------------*/
	
	$("ul.sub-menu").closest("li").addClass("drop");
	
});

/*-----------------------------------------------------------------------------------*/
/* Animate Header Submenus */
/*-----------------------------------------------------------------------------------*/

jQuery('header .menu li').hover(function() {	
		submenu = jQuery('> .sub-menu', this);
		top_distance = jQuery(this).parent().is('header .menu') ? '65px' : '40px';
		submenu.stop().css({overflow:"hidden", height:"auto", display:"none", top: top_distance}).fadeIn({ duration: 200, queue: false }).animate({top: '-=30px'}, 200, function()
		{
			jQuery(this).css({overflow:"visible"});
		});	
	},
	function() {	
		submenu = jQuery('> .sub-menu', this);
		top_distance = jQuery(this).parent().is('header .menu') ? '25px' : '0';
		submenu.stop().css({overflow:"hidden", height:"auto", top:top_distance}).fadeOut(300, function()
		{	
			jQuery(this).css({display:"none"});
		});
	}
);

/*-----------------------------------------------------------------------------------*/
/* Social Popups */
/*-----------------------------------------------------------------------------------*/
	
function popup(pageURL,title,w,h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}