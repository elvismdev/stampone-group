=== CT Mortgage Calculator ===
Contributors: contempoinc
Donate link: 
Tags: real estate,mortgage calculator, calculator,widget
Requires at least: 3.3
Tested up to: 3.4
Stable tag: 3.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A straightforward and simple mortgage calculator, featuring Sale Price, Interest Rate, Term (years) & Down Payment.

== Description ==

A straightforward and simple mortgage calculator, featuring Sale Price, Interest Rate, Term (years) & Down Payment.

== Installation ==

1. Upload the /ct-mortgage-calculator/ folder to the /wp-content/plugins/ directory.
2. Activate the CT Mortgage Calculator plugin through the "Plugins" menu in WordPress.
3. Configure your settings via Appearance > Widgets > CT Mortgage Calculator.
4. And you're good to go!