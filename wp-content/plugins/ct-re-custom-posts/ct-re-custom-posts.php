<?php
/*
Plugin Name: Contempo Real Estate Custom Posts
Plugin URI: http://contempographicdesign.com
Description: Register real estate custom posts
Version: 1.0.0
Author: Chris Robinson
Author URI: http://contempographicdesign.com
*/

add_action( 'init', 'listings_init' );

function listings_init() {
	$labels = array(
		'name' => _x( 'Listings', 'contempo' ),
		'singular_name' => _x( 'Listing', 'contempo' ),
		'add_new' => _x( 'Add New', 'contempo' ),
		'add_new_item' => __( 'Add New Listing', 'contempo' ),
		'edit_item' => __( 'Edit Listing', 'contempo' ),
		'new_item' => __( 'New Listing', 'contempo' ),
		'view_item' => __( 'View Listing', 'contempo' ),
		'search_items' => __( 'Search Listings', 'contempo' ),
		'not_found' =>  __( 'No listings items found', 'contempo' ),
		'not_found_in_trash' => __( 'No listings found in Trash', 'contempo' ),
		'parent_item_colon' => ''
	);

	$args = array( 'labels' => $labels,
		'label' => __('Listings', 'contempo'),
		'singular_label' => __('Listing', 'contempo'),
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => false,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'listings'),
		'menu_position' => 5,
		'has_archive' => true,
		'taxonomies' => array(''),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail'  )
	); 

	register_post_type( 'listings', $args );
}

add_action("manage_posts_custom_column", "ct_custom_columns");
add_filter("manage_edit-listings_columns", "ct_listings_columns");

// Define columns to filter in the edit posts section
function ct_listings_columns($columns) {
	$columns = array(
		//Create custom columns
		"cb" => "<input type=\"checkbox\" />",
		"image" => "Image",
		"title" => "Address",
		"location" => "Location",
		"price" => "Price",
		"beds" => "Beds",
		"baths" => "Baths",
		"author" => "Agent",
		"ct_status" => "Status",
		"date" => "Listed",
	);
	return $columns;
}

// Output custom columns
function ct_custom_columns($column) {
	global $post;
	if ("ID" == $column) echo $post->ID;
	// Display first image attached to listing
	elseif ("image" == $column) ct_first_image_tn();
	// Display city, state and zipcode taxonomies
	elseif ("location" == $column) {
		$_taxonomy = 'city';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}
		echo ', ';
		$_taxonomy = 'state';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}
		echo ' ';
		$_taxonomy = 'zipcode';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}
	}
	// Display price
	elseif ("price" == $column) echo currency() . number_format(get_post_meta($post->ID, "_ct_price", true));
	// Display beds taxonomy
	elseif ("beds" == $column) {
		$_taxonomy = 'beds';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}	
	}
	// Display baths taxonomy
	elseif ("baths" == $column) {
		$_taxonomy = 'baths';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}	
	}
	// Display status taxonomy
	elseif ("ct_status" == $column) {
		$_taxonomy = 'ct_status';
		$terms = get_the_terms( $post_id, $_taxonomy );
		if ( !empty( $terms ) ) {
			$out = array();
			foreach ( $terms as $c )
				$out[] = "<a href='edit-tags.php?action=edit&taxonomy=$_taxonomy&post_type=listings&tag_ID={$c->term_id}'> " . esc_html(sanitize_term_field('name', $c->name, $c->term_id, 'category', 'display')) . "</a>";
			echo join( ', ', $out );
		}
		else {
			_e('Unspecified', 'contempo');
		}	
	}
}

// Register Testimonial custom post type
add_action( 'init', 'testimonial_init' );

function testimonial_init() {
	$labels = array(
		'name' => _x( 'Testimonials', 'post type general name', 'contempo' ),
		'singular_name' => _x( 'Testimonial', 'post type singular name', 'contempo' ),
		'add_new' => _x( 'Add New', 'testimonial', 'contempo' ),
		'add_new_item' => __( 'Add New Testimonial', 'contempo' ),
		'edit_item' => __( 'Edit Testimonial', 'contempo' ),
		'new_item' => __( 'New Testimonial', 'contempo' ),
		'view_item' => __( 'View Testimonial', 'contempo' ),
		'search_items' => __( 'Search Testimonials', 'contempo' ),
		'not_found' =>  __( 'No Testimonials found', 'contempo' ),
		'not_found_in_trash' => __( 'No Testimonials found in Trash', 'contempo' ),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'testimonials'),
		'menu_position' => 5,
		'has_archive' => true,
		'taxonomies' => array('category', 'post_tag'),
		'supports' => array( 'title', 'editor', 'comments', 'author', 'page-attributes', 'thumbnail' )
	);

	register_post_type( 'testimonial', $args );
}

add_action("manage_posts_custom_column", "ct_custom_testimonial_columns");
add_filter("manage_edit-testimonial_columns", "ct_testimonial_columns");

// Define columns to filter in the edit posts section
function ct_testimonial_columns($columns) {
	$columns = array(
		//Create custom columns
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Person or Company",
		"quote" => "Quote",
		"tags" => "Tags",
		"author" => "Author",
		"date" => "Created",
	);
	return $columns;
}

// Output custom columns
function ct_custom_testimonial_columns($column) {
	global $post;
	if ("ID" == $column) echo $post->ID;
	elseif ("quote" == $column) echo $post->post_content;
}

?>